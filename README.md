# Get-HotfixAll

function to list info on last 5 installed updates on current machine.

**Lists:**
- Status
- Computer Name
- Update Name
- KBArticle
- InstalledOn (with time)
